package com.sqli.gossip;

import java.util.ArrayList;
import java.util.List;

import com.sqli.gossip.models.Person;

public class Gossips {

	// ============================ Good Code =============================
	enum GossipState {
		FromTo, SayTo, Free
	}

	String currentGossip;
	Person currentSourcePerson;
	GossipsManagement management;
	GossipState gossipState = GossipState.Free;
	List<Person> allowedToSpread;

	public Gossips(String... entries) {

		management = new GossipsManagement();
		allowedToSpread = new ArrayList<>();

		for (String entry : entries) {
			String label = entry.split(" ")[0];
			String name = entry.split(" ")[1];
			management.add(PersonFactory.getPerson(label, name));
		}

	}

	public Gossips from(String sourceName) {
		currentSourcePerson = management.getPersonByName(sourceName);
		gossipState = GossipState.FromTo;
		return this;
	}

	public Gossips say(String gossip) {
		currentGossip = gossip;
		gossipState = GossipState.SayTo;
		return this;
	}

	public Gossips to(String destination) {

		// Say to a destination
		if (gossipState == GossipState.SayTo && currentGossip != null) {
			Person person = management.getPersonByName(destination);
			person.addGossip(currentGossip);
			currentGossip = null;
		}

		// From source to destination
		else if (gossipState == GossipState.FromTo && currentSourcePerson != null) {
			Person destinationPerson = management.getPersonByName(destination);
			currentSourcePerson.setDestinationPerson(destinationPerson);
			currentSourcePerson = null;
		}

		gossipState = GossipState.Free;
		return this;
	}

	public String ask(String name) {
		Person person = management.getPersonByName(name);
		return person.tellGossip();
	}

	public void spread() {

		/**
		 * Only a person who has had a gossip in the beginning is allowed to transfer
		 *
		 * If a person acquires a gossip in the current spread it will be maintained
		 * until next spread
		 */
		List<Person> list = management.getAllPersons();
		for (Person sourcePerson : list) {
			if (sourcePerson.hasGossip()) {
				allowedToSpread.add(sourcePerson);
			}
		}

		/**
		 * Now we execute the transfer only for allowed persons
		 */
		for (Person sourcePerson : list) {
			if (allowedToSpread.contains(sourcePerson)) {
				sourcePerson.transferGossip();
			}
		}

		allowedToSpread.clear();
		unlockReceiving();

	}

	private void unlockReceiving() {
		for (Person person : management.getAllPersons()) {
			person.unlockReceive();
		}
	}
	// ============================ End Of Good Code =============================

}
