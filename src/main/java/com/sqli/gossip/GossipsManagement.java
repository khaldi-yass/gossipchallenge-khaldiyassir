package com.sqli.gossip;

import java.util.ArrayList;
import java.util.List;

import com.sqli.gossip.models.Person;

/**
 * 
 * @author Yassir KHALDI
 * 
 *         khaldi.yass@gmail.com
 * 
 *         github.com/khaldi-yass
 *
 */
public class GossipsManagement {

	private List<Person> persons;

	public GossipsManagement() {
		persons = new ArrayList<Person>();
	}

	public boolean add(Person person) {
		persons.add(person);
		return true;
	}

	/**
	 * 
	 * @param name
	 * @return object or null;
	 */
	public Person getPersonByName(String name) {
		for (Person person : persons) {
			if (person.getName().equals(name))
				return person;
		}

		return null;
	}

	public List<Person> getAllPersons() {
		return persons;
	}

}
