package com.sqli.gossip;

import com.sqli.gossip.models.Agent;
import com.sqli.gossip.models.Doctor;
import com.sqli.gossip.models.Lady;
import com.sqli.gossip.models.Mister;
import com.sqli.gossip.models.Person;
import com.sqli.gossip.models.Professor;
import com.sqli.gossip.models.Sir;

public class PersonFactory {

	public static Person getPerson(String label, String name) {

		if (label.equals("Mr"))
			return new Mister(name);
		else if (label.equals("Dr"))
			return new Doctor(name);
		else if (label.equals("Agent"))
			return new Agent(name);
		else if (label.equals("Lady"))
			return new Lady(name);
		else if (label.equals("Pr"))
			return new Professor(name);
		else if (label.equals("Sir"))
			return new Sir(name);
		else
			return null;

	}
}
