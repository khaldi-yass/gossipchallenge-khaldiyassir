package com.sqli.gossip.models;

import java.util.ArrayList;
import java.util.List;

public class Agent extends Person {

	// Agent only takes not transfer
	private List<String> gossips;
	private boolean isTransferBlocked;

	public Agent(String n) {
		super(n);
		gossips = new ArrayList<>();
		isTransferBlocked = true;
	}

	@Override
	public boolean addGossip(String gossip) {
		gossips.add(gossip);
		return true;
	}

	@Override
	public boolean transferGossip() {
		// Agent doesn't transfer a gossip
		if (!isTransferBlocked) {
			reset();
		}
		return true;
	}

	@Override
	public boolean hasGossip() {
		// This returns true to allow being called in spread()
		return true;
	}

	@Override
	public String tellGossip() {

		isTransferBlocked = false;
		if (gossips.isEmpty())
			return "";
		else {
			StringBuilder builder = new StringBuilder();
			for (String string : gossips) {
				builder.append(string + ", ");
			}
			return builder.toString().substring(0, builder.length() - 2);
		}
	}

	@Override
	public void reset() {
		gossips.clear();
	}

	@Override
	public boolean isReceiveBlocked() {
		return receiveBlocked;
	}

	@Override
	public void unlockReceive() {
		this.receiveBlocked = false;
	}
	// ===========================================================

}
