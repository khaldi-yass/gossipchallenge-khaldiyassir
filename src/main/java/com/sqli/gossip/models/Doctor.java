package com.sqli.gossip.models;

import java.util.ArrayList;
import java.util.List;

public class Doctor extends Person {

	private List<String> gossips;
	private int cursor;

	public Doctor(String n) {
		super(n);
		gossips = new ArrayList<>();
		cursor = 0;
	}

	@Override
	public boolean addGossip(String gossip) {
		if (!isReceiveBlocked()) {
			gossips.add(gossip);
			receiveBlocked = true;
			return true;
		}
		return false;
	}

	@Override
	public boolean transferGossip() {
		boolean transferSuccess = false;
		if (destinationPerson != null && hasGossip()) {

			// overloading the addGossip method for the types (Lady and Sir)
			// Special casts needed to recognize the method
			if ((destinationPerson instanceof Lady)) {
				transferSuccess = ((Lady) destinationPerson).addGossip(getLatestGossip(), this);
			} else if ((destinationPerson instanceof Sir)) {
				transferSuccess = ((Sir) destinationPerson).addGossip(getLatestGossip(), this);
			} else {
				transferSuccess = destinationPerson.addGossip(getLatestGossip());
			}

			if (transferSuccess) {
				reset();
			}

		}
		return transferSuccess;
	}

	@Override
	public boolean hasGossip() {
		return this.cursor <= gossips.size() - 1;
	}

	@Override
	public String tellGossip() {
		if (gossips.isEmpty())
			return "";
		else {
			StringBuilder builder = new StringBuilder();
			for (String string : gossips) {
				builder.append(string + ", ");
			}
			return builder.toString().substring(0, builder.length() - 2);
		}
	}

	@Override
	public void reset() {
		this.cursor++;
	}

	@Override
	public boolean isReceiveBlocked() {
		return receiveBlocked;
	}

	@Override
	public void unlockReceive() {
		this.receiveBlocked = false;
	}

	/**
	 * A variant of tellGossip() for the Doctor;
	 *
	 * Returns the latest gossip instead of the whole list
	 *
	 * Used only by the transfer method
	 *
	 * @return
	 */
	private String getLatestGossip() {
		if (hasGossip())
			return this.gossips.get(cursor);
		return "";
	}
	// ===========================================================

}
