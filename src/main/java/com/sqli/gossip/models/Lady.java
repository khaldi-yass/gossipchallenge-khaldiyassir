package com.sqli.gossip.models;

public class Lady extends Person {

	// only spread if coming from doctor
	private String gossip;
	private Person sourceGossip;

	public Lady(String n) {
		super(n);
	}

	@Override
	public boolean addGossip(String gossip) {
		if (!isReceiveBlocked()) {
			this.gossip = gossip;
			this.receiveBlocked = true;
			return true;
		}
		return false;
	}

	// a special transfer method for the type lady with the source person
	public boolean addGossip(String gossip, Person person) {
		if (addGossip(gossip)) {
			this.sourceGossip = person;
			return true;
		}
		return false;
	}

	@Override
	public boolean transferGossip() {
		boolean transferSuccess = false;
		// transfer only if source is doctor
		if (destinationPerson != null && hasGossip() && sourceGossip instanceof Doctor) {

			// overloading the addGossip method for the types (Lady and Sir)
			// Special casts needed to recognize the method
			if ((destinationPerson instanceof Lady)) {
				transferSuccess = ((Lady) destinationPerson).addGossip(tellGossip(), this);
			} else if ((destinationPerson instanceof Sir)) {
				transferSuccess = ((Sir) destinationPerson).addGossip(tellGossip(), this);
			} else {
				transferSuccess = destinationPerson.addGossip(tellGossip());
			}

			if (transferSuccess) {
				reset();
			}

		}
		return transferSuccess;

	}

	@Override
	public boolean hasGossip() {
		return this.gossip != null;
	}

	@Override
	public String tellGossip() {
		if (gossip == null)
			return "";
		else
			return gossip;
	}

	@Override
	public void reset() {
		this.gossip = null;
		// receiveBlocked = false;
	}

	@Override
	public boolean isReceiveBlocked() {
		return receiveBlocked;
	}

	@Override
	public void unlockReceive() {
		this.receiveBlocked = false;
	}
	// ===========================================================

}
