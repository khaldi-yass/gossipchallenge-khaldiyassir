package com.sqli.gossip.models;

public abstract class Person {

	protected String name;
	protected Person destinationPerson;
	protected boolean receiveBlocked;

	public String getName() {
		return name;
	}

	public Person(String name) {
		super();
		this.name = name;

	}

	public Person getDestinationPerson() {
		return destinationPerson;
	}

	public void setDestinationPerson(Person destinationPerson) {
		this.destinationPerson = destinationPerson;
	}

	/**
	 * Return gossip in the proper format
	 */
	public abstract String tellGossip();

	/**
	 * Send Gossip
	 */
	public abstract boolean transferGossip();

	/**
	 * Receive Gossip
	 *
	 * Returns true of success operation;
	 *
	 * Returns false if gossip not added (failure);
	 */
	public abstract boolean addGossip(String currentGossip);

	/**
	 * Resetting gossip (unblocking receive)
	 */
	public abstract void reset();

	public abstract boolean isReceiveBlocked();

	public abstract boolean hasGossip();

	public abstract void unlockReceive();

}
