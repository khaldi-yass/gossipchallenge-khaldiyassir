package com.sqli.gossip.models;

public class Professor extends Person {

	// message delayed by one turn
	private String gossip;
	private int currentTurn;

	public Professor(String n) {
		super(n);
		currentTurn = 1;
	}

	@Override
	public boolean addGossip(String gossip) {
		if (!isReceiveBlocked()) {
			this.gossip = gossip;
			receiveBlocked = true;
			return true;
		}
		return false;
	}

	@Override
	public boolean transferGossip() {

		boolean transferSuccess = false;
		// hold transfer until turn 2
		if (destinationPerson != null && hasGossip() && currentTurn == 2) {

			// overloading the addGossip method for the types (Lady and Sir)
			// Special casts needed to recognize the method
			if ((destinationPerson instanceof Lady)) {
				transferSuccess = ((Lady) destinationPerson).addGossip(tellGossip(), this);
			} else if ((destinationPerson instanceof Sir)) {
				transferSuccess = ((Sir) destinationPerson).addGossip(tellGossip(), this);
			} else {
				transferSuccess = destinationPerson.addGossip(tellGossip());
			}

			if (transferSuccess) {
				reset();
			}

		}
		currentTurn++;
		return transferSuccess;

	}

	@Override
	public boolean hasGossip() {
		return this.gossip != null;
	}

	@Override
	public String tellGossip() {
		if (gossip == null)
			return "";
		else
			return gossip;
	}

	@Override
	public void reset() {
		this.gossip = null;
		this.currentTurn = 1;
	}

	@Override
	public boolean isReceiveBlocked() {
		return receiveBlocked;
	}

	@Override
	public void unlockReceive() {
		this.receiveBlocked = false;
	}
	// ===========================================================

}
