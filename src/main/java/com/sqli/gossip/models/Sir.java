package com.sqli.gossip.models;

public class Sir extends Person {

	private String gossip;

	public Sir(String n) {
		super(n);
	}

	@Override
	public boolean addGossip(String gossip) {
		if (!isReceiveBlocked()) {
			this.gossip = gossip;
			receiveBlocked = true;

			return true;
		}
		return false;
	}

	// a special transfer method for the type Sir with the source person
	// Source becomes new destination
	public boolean addGossip(String gossip, Person sourcePerson) {
		if (addGossip(gossip)) {
			this.destinationPerson = sourcePerson;
			return true;
		}
		return false;
	}

	@Override
	public boolean transferGossip() {
		boolean transferSuccess = false;
		if (destinationPerson != null && hasGossip()) {

			StringBuilder builder = new StringBuilder(this.gossip);
			this.gossip = builder.reverse().toString();

			// overloading the addGossip method for the types (Lady and Sir)
			// Special casts needed to recognize the method
			if ((destinationPerson instanceof Lady)) {
				transferSuccess = ((Lady) destinationPerson).addGossip(tellGossip(), this);
			} else if ((destinationPerson instanceof Sir)) {
				transferSuccess = ((Sir) destinationPerson).addGossip(tellGossip(), this);
			} else {
				transferSuccess = destinationPerson.addGossip(tellGossip());
			}

			if (transferSuccess) {
				reset();
			}

		}
		return transferSuccess;

	}

	@Override
	public boolean hasGossip() {
		return this.gossip != null;
	}

	@Override
	public String tellGossip() {
		if (gossip == null)
			return "";
		else
			return gossip;
	}

	@Override
	public void reset() {
		this.gossip = null;
		// receiveBlocked = false;
	}

	@Override
	public boolean isReceiveBlocked() {
		return receiveBlocked;
	}

	@Override
	public void unlockReceive() {
		this.receiveBlocked = false;
	}
	// ===========================================================

}
